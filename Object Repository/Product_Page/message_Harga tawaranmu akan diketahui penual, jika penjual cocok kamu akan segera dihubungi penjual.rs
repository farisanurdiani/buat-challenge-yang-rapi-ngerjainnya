<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>message_Harga tawaranmu akan diketahui penual, jika penjual cocok kamu akan segera dihubungi penjual</name>
   <tag></tag>
   <elementGuidId>b5218b70-a924-42ce-bd42-0a14c7d04291</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>div.modal-body > p</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>(.//*[normalize-space(text()) and normalize-space(.)='Masukkan Harga Tawarmu'])[1]/following::p[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>p</value>
      <webElementGuid>4fa45a94-a0eb-4972-a90c-87fae213f834</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Harga tawaranmu akan diketahui penual, jika penjual cocok kamu akan segera dihubungi penjual.</value>
      <webElementGuid>54b30954-9b22-44b0-9e58-ff8218e7a73f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>/html[1]/body[@class=&quot;modal-open&quot;]/div[@class=&quot;fade modal show&quot;]/div[@class=&quot;modal-dialog modal-sm modal-dialog-centered&quot;]/div[@class=&quot;modal-content&quot;]/div[@class=&quot;modal-body&quot;]/p[1]</value>
      <webElementGuid>864403b8-4203-4c25-8dab-fb02c6a60698</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Masukkan Harga Tawarmu'])[1]/following::p[1]</value>
      <webElementGuid>610a80af-5241-4bf2-a741-b39fa6b47d86</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='`'])[2]/following::p[1]</value>
      <webElementGuid>9dac8829-a231-4807-b5c4-556b3d60adab</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Action Figures Anime'])[2]/preceding::p[1]</value>
      <webElementGuid>7e116c99-ec8d-4ef8-88a8-bcda1affb20c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Harga Tawar'])[1]/preceding::p[2]</value>
      <webElementGuid>ac389d13-a1f9-47d1-b69c-2ee55da51f1b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Harga tawaranmu akan diketahui penual, jika penjual cocok kamu akan segera dihubungi penjual.']/parent::*</value>
      <webElementGuid>ffec5ea6-64a2-4e5c-87cd-5279cfa01f44</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[4]/div/div/div[2]/p</value>
      <webElementGuid>06019a46-599c-4cfc-8c43-9e926fa73661</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//p[(text() = 'Harga tawaranmu akan diketahui penual, jika penjual cocok kamu akan segera dihubungi penjual.' or . = 'Harga tawaranmu akan diketahui penual, jika penjual cocok kamu akan segera dihubungi penjual.')]</value>
      <webElementGuid>f4b9a67c-2f9b-471f-b05c-07487b30f024</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
