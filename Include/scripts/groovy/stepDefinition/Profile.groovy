package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When

import internal.GlobalVariable

public class Profile {
@When("user click the icon in the very top right corner")
public void user_click_the_icon_in_the_very_top_right_corner() {
	WebUI.click(findTestObject('Home_page_login/button_icon_profile'))
}

@When("user Choose Profile menu")
public void user_Choose_Profile_menu() {
	WebUI.click(findTestObject('Home_page_login/button_profile'))
	WebUI.verifyElementVisible(findTestObject('Profile_page/label_alamat'))
}

@When("user delete and enter new data in Name field with {string}")
public void user_delete_and_enter_new_data_in_Name_field_with(String nama) {
	WebUI.setText(findTestObject('Profile_page/input_nama'),nama )
}

@When("user edit a City by clicking the City dropdown list {string}")
public void user_edit_a_City_by_clicking_the_City_dropdown_list(String kota) {
	WebUI.selectOptionByIndex(findTestObject('Profile_page/select_kota', [('1') : 'Bandung', ('2') : 'Bogor', ('3') : 'Jember'
			, ('4') : 'Kediri', ('5') : 'Lumajang', ('6') : 'Malang', ('7') : 'Pasuruan', ('8') : 'Probolinggo', ('9') : 'Yogyakarta']),
		kota)
}

@When("user delete and enter new address in Address field {string}")
public void user_delete_and_enter_new_address_in_Address_field(String alamat) {
	WebUI.setText(findTestObject('Profile_page/input_alamat'),alamat )
}

@When("user delete and enter new phone number in Phone Number field {string}")
public void user_delete_and_enter_new_phone_number_in_Phone_Number_field(String number) {
	WebUI.setText(findTestObject('Profile_page/input_phone'),number )
}

@When("user click Submit button")
public void user_click_Submit_button() {
	WebUI.click(findTestObject('Profile_page/button_submit'))
}

@Then("user can see successful pop up message")
public void user_can_see_successful_pop_up_message() {
	WebUI.verifyElementVisible(findTestObject('Profile_page/message_berhasil edit'))
}
}
