package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import org.openqa.selenium.ElementNotSelectableException

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows

import ch.qos.logback.core.joran.conditional.ElseAction
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import groovy.json.StringEscapeUtils
import internal.GlobalVariable

public class Login {
	@Given("User go to login page")
	public void user_go_to_login_page() {
		WebUI.openBrowser('')
		WebUI.maximizeWindow()
		WebUI.navigateToUrl('https://secondhand-store.herokuapp.com/')
		WebUI.verifyElementVisible(findTestObject('Home_page_login/banner_diskon_ramadhan'))
		WebUI.verifyElementVisible(findTestObject('Home_page_login/button_kategori_baju'))
		WebUI.click(findTestObject('Home_page_not_login/button_masuk'))
		WebUI.verifyElementVisible(findTestObject('login_page/banner_secondhand login'))
		WebUI.verifyElementVisible(findTestObject('login_page/title_masuk'))
	}

	@When("User click Masuk button")
	public void user_click_Masuk_button() {
		WebUI.click(findTestObject('login_page/button_masuk'))
	}

	@Then("User (.*) login")
	public void user_login(String result) {
		if(result=='successfully') {
			WebUI.verifyElementVisible(findTestObject('Home_page_login/banner_diskon_ramadhan'))
			WebUI.verifyElementVisible(findTestObject('Home_page_login/button_kategori_baju'))
		}else if(result=='failed') {
			WebUI.verifyElementVisible(findTestObject('login_page/banner_secondhand login'))
			WebUI.verifyElementVisible(findTestObject('login_page/title_masuk'))
		}
	}

	@When("User input email field with (.*) email")
	public void user_input_email_field_with_email(String email){
		if(email=='valid') {
			WebUI.setText(findTestObject('login_page/input_email'), 'gumiwang.nc@gmail.com')
		}else if(email=='invalid') {
			WebUI.setText(findTestObject('login_page/input_email'), 'gumiwangbinar@gmail.com')
		}else if(email=='empty') {
			WebUI.setText(findTestObject('login_page/input_email'), '')
		}
	}

	@When("User input password field with (.*) password")
	public void user_input_password_field_with_password(String password) {
		if(password=='valid') {
			WebUI.setText(findTestObject('login_page/input_password'), '987654321')
		}else if(password=='invalid') {
			WebUI.setText(findTestObject('login_page/input_password'), '123456789')
		}else if(password=='empty') {
			WebUI.setText(findTestObject('login_page/input_password'), '')
		}
	}

	@When("User input email field with {string}")
	public void user_input_email_field_with(String email) {
		WebUI.setText(findTestObject('login_page/input_email'), email)
	}
	@When("User input password field with {string}")
	public void user_input_password_field_with(String password) {
		WebUI.setText(findTestObject('login_page/input_password'), password)
	}
}
