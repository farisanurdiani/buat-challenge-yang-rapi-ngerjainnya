package stepDefinition

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When
import internal.GlobalVariable

public class Editproduct {

	@Given("User click daftar jual saya button")
	public void user_click_daftar_jual_saya_button() {
		WebUI.click(findTestObject('Home_page_login/button_daftarjual'))
	}

	@When("User click product card")
	public void user_click_product_card() {
		WebUI.click(findTestObject('EditProduct_Page/button_duitKesehatanRp5.000,00'))
	}

	@When("User click edit button")
	public void user_click_edit_button() {
		WebUI.click(findTestObject('EditProduct_Page/button_Edit'))
	}

	@When("User input product name")
	public void user_input_product_name() {
		WebUI.setText(findTestObject('Productinfo_page/input_NamaProduk'), 'ayam')
	}

	@When("User input price product with {string}")
	public void user_input_price_product_with(String price) {
		WebUI.setText(findTestObject('Productinfo_page/input_HargaProduk'), price)
	}


	@When("User choose Kendaraan product category")
	public void user_choose_Kendaraan_product_category() {
		WebUI.selectOptionByValue(findTestObject('Productinfo_page/select_Kategori'), '2', false)
	}

	@When("User input product description")
	public void user_input_product_description() {
		WebUI.setText(findTestObject('Productinfo_page/input_Deskripsi'), 'warna warni')
	}

	@When("User click button terbitkan")
	public void user_click_button_terbitkan() {
		WebUI.click(findTestObject('Productinfo_page/button_Terbitkan'))
	}

	@Then("User successfully edit product")
	public void user_successfully_edit_product() {
		WebUI.verifyElementVisible(findTestObject('Home_page_login/banner_diskon_ramadhan'))
	}

	@Then("User failed edit product")
	public void user_failed_edit_product() {
		WebUI.verifyElementVisible(findTestObject('Productinfo_page/button_Terbitkan'))
	}

	@When("User choose empty product category")
	public void user_choose_empty_product_category() {
		WebUI.selectOptionByLabel(findTestObject('Productinfo_page/select_Kategori'), 'Pilih Kategori', false)
	}
}
