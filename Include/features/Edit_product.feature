@Edit_product
Feature: edit product

  Background: user already login
    Given User go to login page
    When User input email field with valid email
    And User input password field with valid password
    And User click Masuk button
    And User successfully login

    
  Scenario Outline: User can edit product with <condition>
   	When User click daftar jual saya button
   	And User click product card
   	And User click edit button
   	And User input product name
   	And User input price product with <price>   
   	And User choose <category> product category  
   	And User input product description
   	And User click button terbitkan
   	Then User <result> edit product
   	
   	Examples:
   	 	| case_id | condition           			| price    	| category 	| result			|
			|	E01			| successfully  						| '8000'		| Kendaraan	| successfully|
     	| E02     | with alphabet price    		| 'zzzz'		| Kendaraan	| failed			|
     	| E03     | with 0 price							| '0'				| Kendaraan	| successfully|
     	| E04     | with <0								 		| '-8000'		| Kendaraan | successfully|
     	| E05     | without select category		| '8000'		| empty			| failed			|  
