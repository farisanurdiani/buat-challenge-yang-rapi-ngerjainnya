@Add_Product
Feature: Add Product

  Background: User do login process
    Given User go to login page
    When User input email field with "rendyyudaa@gmail.com"
    And User input password field with "Lahir12345"
    And User click Masuk button
    And User successfully login

   Scenario Outline: User can add product <condition>
   	When User click jual button
   	And User input product name with "Esemka 4 x 4" 
   	And User input product price with <price>   
   	And User choose <category> on product category  
   	And User input product description with <desc> 
   	And User click terbitkan button
   	Then User <result> add product
   	
   	Examples:
   	 | case_id | condition           				| price    | category | desc        | result			|
     | A01     | successfully  						  | '9000000'| Kendaraan| description | successfully|
     | A02     | with alphabet price    		| 'QWERTY' | Kendaraan| description | failed			|
     | A03     | with one product data empty| ''       | Kendaraan| empty 			| failed			|
     | A04     | without select category 	  | '9000000'| empty		| description | failed			|
     | A05     | with 0 price    	  			  | '0'		   | Kendaraan| description | successfully|
